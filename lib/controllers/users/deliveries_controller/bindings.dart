import 'package:get/get.dart';
import 'package:nextrade/controllers/users/deliveries_controller/controller.dart';

class DeliveriesBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DeliveriesControllerImp>(() => DeliveriesControllerImp());
  }
}
