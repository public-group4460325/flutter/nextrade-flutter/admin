import 'package:nextrade/controllers/users/deliveries_controller/state.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class DeliveriesController extends GetxController {}

class DeliveriesControllerImp extends DeliveriesController {
  final state = DeliveriesState();
}
