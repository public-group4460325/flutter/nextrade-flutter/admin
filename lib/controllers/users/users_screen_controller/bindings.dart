import 'package:get/get.dart';
import 'package:nextrade/controllers/users/users_controller/controller.dart';

class UsersBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UsersControllerImp>(() => UsersControllerImp());
  }
}
