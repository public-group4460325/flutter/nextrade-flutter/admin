import 'package:nextrade/controllers/users/users_screen_controller/state.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class UsersScreenController extends GetxController {
  changePage(int currentPage);
}

class UsersScreenControllerImp extends UsersScreenController {
  final state = UsersScreenState();

  @override
  changePage(currentPage) {
    state.currentPage = currentPage;
    update();
  }
}
