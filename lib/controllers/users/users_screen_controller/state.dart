import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/view/screens/users/deliveries.dart';
import 'package:nextrade/view/screens/users/users.dart';

class UsersScreenState {
  List<Widget> pagesList = [
    const Users(),
    const Deliveries(),
  ];
  List bottomNavBarActions = [
    {
      'title': 'customers'.tr,
      'icon': LineIcons.user,
    },
    {
      'title': 'deliveries'.tr,
      'icon': LineIcons.truck,
    },
  ];
  int currentPage = 0;
}
