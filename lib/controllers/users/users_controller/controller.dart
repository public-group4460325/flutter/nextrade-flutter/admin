import 'package:nextrade/controllers/users/users_controller/state.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class UsersController extends GetxController {}

class UsersControllerImp extends UsersController {
  final state = UsersState();
}
