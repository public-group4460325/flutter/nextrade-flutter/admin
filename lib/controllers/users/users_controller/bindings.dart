import 'package:get/get.dart';
import 'package:nextrade/controllers/users/deliveries_controller/controller.dart';
import 'package:nextrade/controllers/users/users_controller/controller.dart';
import 'package:nextrade/controllers/users/users_screen_controller/controller.dart';

class UsersScreenBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<UsersScreenControllerImp>(() => UsersScreenControllerImp());
    Get.put(UsersControllerImp());
    Get.put(DeliveriesControllerImp());
  }
}
