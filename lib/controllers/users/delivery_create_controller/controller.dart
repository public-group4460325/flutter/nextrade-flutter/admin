import 'package:nextrade/controllers/users/delivery_create_controller/state.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class DeliveryCreateController extends GetxController {}

class DeliveryCreateControllerImp extends DeliveryCreateController {
  final state = DeliveryCreateState();
}
