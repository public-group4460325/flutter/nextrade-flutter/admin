import 'package:get/get.dart';
import 'package:nextrade/controllers/users/delivery_create_controller/controller.dart';

class DeliveryCreateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<DeliveryCreateControllerImp>(() => DeliveryCreateControllerImp());
  }
}
