import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../../../core/classes/status_request.dart';
import '../../../core/constants/routes.dart';
import '../../../core/functions/handling_data.dart';
import '../../../core/functions/toast_message.dart';
import 'state.dart';

abstract class LoginController extends GetxController {
  login();
  toForgetPasswordPage();
  showOrHidePassword();
  initData();
}

class LoginControllerImp extends LoginController {
  final state = LoginState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.emailController = TextEditingController();
    state.passwordController = TextEditingController();
  }

  @override
  login() async {
    try {
      if (!state.formState.currentState!.validate()) return;
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.loginData.postData(
        email: state.emailController.text,
        password: state.passwordController.text,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        if (response['data']['user']['user_type_id'] == 'user') {
          toastMessage(message: 'download_users_app'.tr);
          state.statusRequest = StatusRequest.none;
          update();
          return;
        } else if (response['data']['user']['user_type_id'] == 'delivery') {
          toastMessage(message: 'download_deliveries_app'.tr);
          state.statusRequest = StatusRequest.none;
          update();
          return;
        }
        state.myServices.sharedPreferences
            .setString('apiToken', response['data']['token']);
        state.myServices.sharedPreferences
            .setString('id', response['data']['user']['id'].toString());
        state.myServices.sharedPreferences
            .setString('name', response['data']['user']['name']);
        state.myServices.sharedPreferences
            .setString('email', response['data']['user']['email']);
        state.myServices.sharedPreferences.setBool('isLoggedIn', true);
        FirebaseMessaging.instance.subscribeToTopic('admins');
        return Get.offAllNamed(AppRoute.adminHome);
      }
      if (response['msg'] == 'User Not Found') {
        toastMessage(message: 'user_not_found'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Wrong Password') {
        toastMessage(message: 'wrong_password'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toForgetPasswordPage() {
    Get.toNamed(AppRoute.forgetPassword);
  }

  @override
  showOrHidePassword() {
    state.ishHidden = !state.ishHidden;
    update();
  }

  // @override
  // toHomePage() {
  //   FirebaseMessaging.instance.getToken().then((value) {
  //     state.myServices.sharedPreferences.setString('notificationToken', value!);
  //   });
  //   FirebaseMessaging.instance.subscribeToTopic(
  //       'users${state.myServices.sharedPreferences.getString('id')}');
  //   if (state.myServices.sharedPreferences.getString('userType') == 'user') {
  //     FirebaseMessaging.instance.subscribeToTopic('users');
  //     return Get.offAllNamed(AppRoute.homeScreen);
  //   } else if (state.myServices.sharedPreferences.getString('userType') ==
  //       'admin') {
  //     FirebaseMessaging.instance.subscribeToTopic('admins');
  //     return Get.offAllNamed(AppRoute.adminHome);
  //   } else if (state.myServices.sharedPreferences.getString('userType') ==
  //       'delivery') {
  //     FirebaseMessaging.instance.subscribeToTopic('deliveries');
  //     return Get.offAllNamed(AppRoute.deliveryHomeScreen);
  //   }
  // }

  @override
  void dispose() {
    state.emailController.dispose();
    state.passwordController.dispose();
    super.dispose();
  }
}
