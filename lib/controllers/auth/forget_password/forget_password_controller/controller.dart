import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/classes/status_request.dart';
import '../../../../core/constants/routes.dart';
import '../../../../core/functions/handling_data.dart';
import '../../../../core/functions/toast_message.dart';
import 'state.dart';

abstract class ForgetPasswordController extends GetxController {
  initData();
  checkMail();
  toVerifyCodePage();
}

class ForgetPasswordControllerImp extends ForgetPasswordController {
  final state = ForgetPasswordState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.emailController = TextEditingController();
  }

  @override
  checkMail() async {
    try {
      if (!state.formState.currentState!.validate()) return;
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.checkEmailData.postData(
        email: state.emailController.text,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toVerifyCodePage();
      }
      if (response['msg'] == 'User Not Found') {
        toastMessage(message: 'user_not_found'.tr);
        update();
        return;
      }
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toVerifyCodePage() {
    Get.toNamed(AppRoute.verifyCodeForgetPassword, arguments: {'email': state.emailController.text});
  }

  @override
  void dispose() {
    state.emailController.dispose();
    super.dispose();
  }
}
