import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/classes/status_request.dart';
import '../../../../data/data_source/remote/auth/forget_password/reset_password_data.dart';

class ResetPasswordState {
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  late TextEditingController passwordController;
  late TextEditingController confirmPasswordController;
  StatusRequest statusRequest = StatusRequest.none;
  ResetPasswordData resetPasswordData = ResetPasswordData(Get.find());
  bool ishHidden = true;
  late String email;
}
