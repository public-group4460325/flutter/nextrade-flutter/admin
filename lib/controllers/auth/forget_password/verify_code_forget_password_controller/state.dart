import 'package:get/get.dart';

import '../../../../core/classes/status_request.dart';
import '../../../../data/data_source/remote/auth/forget_password/verify_code_forget_password_data.dart';

class VerifyCodeForgetPasswordState {
  late String email;
  StatusRequest statusRequest = StatusRequest.none;
  VerifyCodeForgetPasswordData verifyCodeForgetPasswordData =
      VerifyCodeForgetPasswordData(Get.find());
}
