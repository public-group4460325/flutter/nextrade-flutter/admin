import 'package:get/get.dart';

import 'controller.dart';

class VerifyCodeForgetPasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VerifyCodeForgetPasswordControllerImp>(() => VerifyCodeForgetPasswordControllerImp());
  }
}
