import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/view/screens/orders/accepted_orders.dart';
import 'package:nextrade/view/screens/orders/archived_orders.dart';
import 'package:nextrade/view/screens/orders/pending_orders.dart';

class OrdersScreenState {
  List<Widget> pagesList = [
    const AdminPendingOrders(),
    const AdminAcceptedOrders(),
    const AdminArchivedOrders(),
  ];

  List bottomNavBarActions = [
    {
      'title': 'pending_orders'.tr,
      'icon': LineIcons.clock,
    },
    {
      'title': 'accepted_orders'.tr,
      'icon': LineIcons.check,
    },
    {
      'title': 'archived_orders'.tr,
      'icon': LineIcons.archive,
    },
  ];
  int currentPage = 0;
}
