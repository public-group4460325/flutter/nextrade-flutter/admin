import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/orders_screen_controller/controller.dart';
import 'package:nextrade/controllers/orders/accepted_orders_controller/controller.dart';
import 'package:nextrade/controllers/orders/archived_orders_controller/controller.dart';
import 'package:nextrade/controllers/orders/pending_orders_controller/controller.dart';

class OrdersScreenBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrdersScreenControllerImp>(
        () => OrdersScreenControllerImp());
    Get.put(PendingOrdersControllerImp());
    Get.put(AcceptedOrdersControllerImp());
    Get.put(ArchivedOrdersControllerImp());
  }
}
