import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/orders_screen_controller/state.dart';

abstract class  OrdersScreenController extends GetxController {
  changePage(int currentPage);
}

class  OrdersScreenControllerImp extends  OrdersScreenController {
  final state =  OrdersScreenState();

  @override
  changePage(currentPage) {
    state.currentPage = currentPage;
    update();
  }
}
