import 'dart:async';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/controllers/orders/order_details_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/address_model.dart';
import 'package:nextrade/data/models/item_model.dart';

abstract class OrderDetailsController extends GetxController {
  initData();
  getData();
}

class OrderDetailsControllerImp extends OrderDetailsController {
  final state = OrderDetailsState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.order = Get.arguments['order'];
    state.controller = Completer<GoogleMapController>();
    state.lat = 0;
    state.long = 0;
    state.kGooglePlex =
        const CameraPosition(target: LatLng(0, 0), zoom: 12.4746);
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.ordersData.getDetailsOrder(
        state.order.id!,
        state.apiToken,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.items.clear();
        state.items.addAll(
            response['data']['items'].map<Item>((e) => Item.fromJson(e)));
        if (!response['data']['address'].isEmpty) {
          state.address = Address.fromJson(response['data']['address']);
        }
        if (state.address != null) {
          state.lat = state.address!.locationLat!;
          state.long = state.address!.locationLong!;
          state.markers.clear();
          state.markers.add(
            Marker(
              markerId: const MarkerId('1'),
              position: LatLng(
                state.lat,
                state.long,
              ),
            ),
          );
          state.kGooglePlex = CameraPosition(
              target: LatLng(
                state.lat,
                state.long,
              ),
              zoom: 12.4746);
        }
        state.items.forEach((element) => state.totalPrice += element.price);
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }
}
