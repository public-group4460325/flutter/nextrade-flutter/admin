import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/accepted_orders_controller/state.dart';
import 'package:nextrade/controllers/orders/archived_orders_controller/controller.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/order_model.dart';

abstract class AcceptedOrdersController extends GetxController {
  initData();
  getOrders();
  prepareOrder(int orderId);
  String getOrderStatus(String val);
  String getReceiveType(String val);
  String getPaymentType(String val);
  toOrdersDetailsPage(Order order);
  refreshPage();
}

class AcceptedOrdersControllerImp extends AcceptedOrdersController {
  final state = AcceptedOrdersState();

  @override
  void onInit() {
    initData();
    getOrders();
    super.onInit();
  }

  @override
  void initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    super.onInit();
  }

  @override
  getOrders() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.ordersData.getAcceptedOrders(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.orders.addAll(response['data'].map((e) => Order.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  prepareOrder(orderId) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response =
          await state.ordersData.prepareOrder(orderId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        Order temp =
            state.orders.firstWhere((element) => element.id == orderId);
        if (temp.receiveType == 'drive_thru') {
          ArchivedOrdersControllerImp controller = Get.find();
          controller.state.orders.add(temp);
          state.orders.remove(temp);
        } else {
          temp.status = 2;
        }
        if (state.orders.isEmpty) state.statusRequest = StatusRequest.noData;
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  String getOrderStatus(val) {
    if (val == '0') return 'await_approval'.tr;
    if (val == '1') return 'preparing'.tr;
    if (val == '2') return 'received_by_delivery'.tr;
    if (val == '3') return 'on_the_way'.tr;
    return 'archived'.tr;
  }

  @override
  String getReceiveType(val) {
    if (val == 'drive_thru') return 'drive_thru'.tr;
    if (val == 'delivery') return 'delivery'.tr;
    return '';
  }

  @override
  String getPaymentType(val) {
    if (val == 'cash') return 'cash_on_delivery'.tr;
    if (val == 'cards') return 'payment_cards'.tr;
    return '';
  }

  @override
  toOrdersDetailsPage(order) {
    Get.toNamed(AppRoute.orderDetails, arguments: {'order': order});
  }

  @override
  refreshPage() {
    getOrders();
  }
}
