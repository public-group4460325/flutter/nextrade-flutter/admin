import 'dart:io';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/categories_data.dart';
import 'package:nextrade/data/models/category_model.dart';

class CategoriesUpdateState {
  CategoriesData categoriesData = CategoriesData(Get.find());
  late StatusRequest statusRequest;
  late String apiToken;
  MyServices myServices = Get.find();
  late TextEditingController nameArController;
  late TextEditingController nameEnController;
  File? image;
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  late Category category;
}
