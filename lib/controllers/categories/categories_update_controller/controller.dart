import 'package:flutter/material.dart';
import 'package:nextrade/controllers/categories/categories_update_controller/state.dart';
import 'package:nextrade/controllers/categories/categories_view_controller/controller.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/core/functions/upload_file.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class CategoriesUpdateController extends GetxController {
  initData();
  updateCategory();
  toCategoriesViewPage();
  chooseImage();
}

class CategoriesUpdateControllerImp extends CategoriesUpdateController {
  final state = CategoriesUpdateState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.category = Get.arguments['category'];
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.statusRequest = StatusRequest.none;
    state.nameArController = TextEditingController();
    state.nameArController.text = state.category.nameAr!;
    state.nameEnController = TextEditingController();
    state.nameEnController.text = state.category.nameEn!;
  }

  @override
  updateCategory() async {
    try {
      if (state.formState.currentState!.validate()) {
        state.statusRequest = StatusRequest.loading;
        update();
        var response = await state.categoriesData.updateCategory(
          categoryId: state.category.id!,
          nameAr: state.nameArController.text,
          nameEn: state.nameEnController.text,
          apiToken: state.apiToken,
          image: state.image,
        );
        state.statusRequest = handlingData(response);
        if (StatusRequest.success != state.statusRequest) {
          update();
          return;
        }
        if (response['status']) {
          CategoriesViewControllerImp controller = Get.find();
          controller.getData();
          toCategoriesViewPage();
          update();
          return;
        }
        toastMessage(message: response['msg']);
      }
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toCategoriesViewPage() {
    Get.back();
  }

  @override
  chooseImage() async {
    state.image = await uploadImageGallery(isSvg: true);
    update();
  }
}
