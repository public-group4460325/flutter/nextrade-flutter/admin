import 'package:get/get.dart';
import 'package:nextrade/controllers/categories/categories_update_controller/controller.dart';

class CategoriesUpdateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CategoriesUpdateControllerImp>(() => CategoriesUpdateControllerImp());
  }
}
