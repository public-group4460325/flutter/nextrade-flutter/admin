import 'package:nextrade/controllers/categories/categories_view_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/category_model.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class CategoriesViewController extends GetxController {
  initData();
  getData();
  deleteCategory(int categoryId);
  toCategoryCreatePage();
  toCategoriesUpdatePage(int categoryId);
  toItemsViewPage(int categoryId);
}

class CategoriesViewControllerImp extends CategoriesViewController {
  final state = CategoriesViewState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.statusRequest = StatusRequest.none;
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response =
          await state.categoriesData.getAllCategories(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.data.clear();
        state.data.addAll(response['data'].map((e) => Category.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  deleteCategory(categoryId) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response =
          await state.categoriesData.deleteCategory(categoryId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.data.removeWhere((element) => element.id == categoryId);
        if (state.data.isEmpty) state.statusRequest = StatusRequest.noData;
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toCategoryCreatePage() {
    Get.toNamed(AppRoute.categoriesCreate);
  }

  @override
  toCategoriesUpdatePage(categoryId) {
    Category category =
        state.data.firstWhere((element) => element.id == categoryId);
    Get.toNamed(AppRoute.categoriesUpdate, arguments: {'category': category});
  }

  @override
  toItemsViewPage(int categoryId) {
    Get.toNamed(AppRoute.itemsView, arguments: {'categoryId': categoryId});
  }
}
