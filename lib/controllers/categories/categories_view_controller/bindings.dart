import 'package:get/get.dart';
import 'package:nextrade/controllers/categories/categories_view_controller/controller.dart';

class CategoriesViewBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CategoriesViewControllerImp>(() => CategoriesViewControllerImp());
  }
}
