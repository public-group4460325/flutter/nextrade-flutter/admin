import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/categories_data.dart';
import 'package:nextrade/routes/routes_links.dart';

class CategoriesViewState {
  CategoriesData categoriesData = CategoriesData(Get.find());
  List data = [];
  late StatusRequest statusRequest;
  late String apiToken;
  MyServices myServices = Get.find();
}
