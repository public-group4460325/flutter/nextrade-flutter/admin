import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/categories_data.dart';

class CategoriesCreateState {
  CategoriesData categoriesData = CategoriesData(Get.find());
  late StatusRequest statusRequest;
  late String apiToken;
  MyServices myServices = Get.find();
  late TextEditingController nameArController;
  late TextEditingController nameEnController;
  File? image;
  GlobalKey<FormState> formState = GlobalKey<FormState>();
}
