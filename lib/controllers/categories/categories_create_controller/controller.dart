import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/categories/categories_create_controller/state.dart';
import 'package:nextrade/controllers/categories/categories_view_controller/controller.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/core/functions/upload_file.dart';

abstract class CategoriesCreateController extends GetxController {
  initData();
  createCategory();
  toCategoriesViewPage();
  chooseImage();
}

class CategoriesCreateControllerImp extends CategoriesCreateController {
  final state = CategoriesCreateState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.statusRequest = StatusRequest.none;
    state.nameArController = TextEditingController();
    state.nameEnController = TextEditingController();
  }

  @override
  createCategory() async {
    try {
      if (state.image == null) {
        return toastMessage(message: 'select_image_first'.tr);
      }
      if (state.formState.currentState!.validate()) {
        state.statusRequest = StatusRequest.loading;
        update();
        var response = await state.categoriesData.createCategory(
          nameAr: state.nameArController.text,
          nameEn: state.nameEnController.text,
          apiToken: state.apiToken,
          image: state.image!,
        );
        state.statusRequest = handlingData(response);
        if (StatusRequest.success != state.statusRequest) {
          update();
          return;
        }
        if (response['status']) {
          CategoriesViewControllerImp controller = Get.find();
          controller.getData();
          toCategoriesViewPage();
          update();
          return;
        }
        toastMessage(message: response['msg']);
      }
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toCategoriesViewPage() {
    Get.back();
  }

  @override
  chooseImage() async {
    state.image = await uploadImageGallery(isSvg: true);
    update();
  }
}
