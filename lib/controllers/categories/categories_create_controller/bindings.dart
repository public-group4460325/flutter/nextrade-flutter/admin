import 'package:get/get.dart';
import 'package:nextrade/controllers/categories/categories_create_controller/controller.dart';

class CategoriesCreateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CategoriesCreateControllerImp>(() => CategoriesCreateControllerImp());
  }
}
