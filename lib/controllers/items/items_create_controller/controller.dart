import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/material.dart';
import 'package:nextrade/controllers/items/items_create_controller/state.dart';
import 'package:nextrade/controllers/items/items_view_controller/controller.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/core/functions/upload_file.dart';
import 'package:nextrade/data/models/category_model.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class ItemsCreateController extends GetxController {
  initData();
  createItem();
  getCategories();
  toItemViewPage();
  showOptionsImage();
  chooseImageCamera();
  chooseImageGallery();
}

class ItemsCreateControllerImp extends ItemsCreateController {
  final state = ItemsCreateState();

  @override
  void onInit() {
    initData();
    getCategories();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.statusRequest = StatusRequest.none;
    state.nameArController = TextEditingController();
    state.deskArController = TextEditingController();
    state.nameEnController = TextEditingController();
    state.deskEnController = TextEditingController();
    state.countController = TextEditingController();
    state.priceController = TextEditingController();
    state.discountController = TextEditingController();
    state.categoryIdController = TextEditingController();
    state.categoryNameController = TextEditingController();
  }

  @override
  createItem() async {
    try {
      if (state.image == null) {
        return toastMessage(message: 'select_image_first'.tr);
      }
      if (state.formState.currentState!.validate()) {
        state.statusRequest = StatusRequest.loading;
        update();
        var response = await state.itemsData.createItem(
          nameAr: state.nameArController.text,
          deskAr: state.deskArController.text,
          nameEn: state.nameEnController.text,
          deskEn: state.deskEnController.text,
          count: state.countController.text,
          discount: state.discountController.text,
          price: state.priceController.text,
          categoryId: state.categoryIdController.text,
          apiToken: state.apiToken,
          image: state.image!,
        );
        state.statusRequest = handlingData(response);
        if (StatusRequest.success != state.statusRequest) {
          update();
          return;
        }
        if (response['status']) {
          if (state.categoryIdController.text == '${state.categoryId}') {
            ItemsViewControllerImp controller = Get.find();
            controller.getData();
          }
          toItemViewPage();
          update();
          return;
        }
        toastMessage(message: response['msg']);
      }
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  getCategories() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response =
          await state.categoriesData.getAllCategories(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        List temp = [];
        temp.addAll(response['data'].map((e) => Category.fromJson(e)));
        temp.forEach(
          (element) => state.categories.add(
            SelectedListItem(
              name: translateDatabase(element.nameAr, element.nameEn),
              value: '${element.id}',
            ),
          ),
        );
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toItemViewPage() {
    Get.back();
  }

  @override
  showOptionsImage() {
    showBottomMenu(chooseImageCamera, chooseImageGallery);
  }

  @override
  chooseImageCamera() async {
    state.image = await uploadImageCamera();
    update();
  }

  @override
  chooseImageGallery() async {
    state.image = await uploadImageGallery();
    update();
  }
}
