import 'package:get/get.dart';
import 'package:nextrade/controllers/items/items_create_controller/controller.dart';

class ItemsCreateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemsCreateControllerImp>(() => ItemsCreateControllerImp());
  }
}
