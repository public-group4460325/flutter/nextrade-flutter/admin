import 'package:get/get.dart';
import 'package:nextrade/controllers/items/items_update_controller/controller.dart';

class ItemsUpdateBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemsUpdateControllerImp>(() => ItemsUpdateControllerImp());
  }
}
