import 'dart:io';
import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/categories_data.dart';
import 'package:nextrade/data/data_source/remote/items_data.dart';
import 'package:nextrade/data/models/item_model.dart';

class ItemsUpdateState {
  CategoriesData categoriesData = CategoriesData(Get.find());
  ItemsData itemsData = ItemsData(Get.find());
  List<SelectedListItem> categories = [];
  late StatusRequest statusRequest;
  late String apiToken;
  MyServices myServices = Get.find();
  late Item item;
  late TextEditingController nameArController;
  late TextEditingController deskArController;
  late TextEditingController nameEnController;
  late TextEditingController deskEnController;
  late TextEditingController countController;
  late TextEditingController priceController;
  late TextEditingController discountController;
  late TextEditingController categoryIdController;
  late TextEditingController categoryNameController;
  File? image;
  late int isActive;
  GlobalKey<FormState> formState = GlobalKey<FormState>();
}
