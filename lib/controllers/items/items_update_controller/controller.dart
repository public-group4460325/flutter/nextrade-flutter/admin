import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/material.dart';
import 'package:nextrade/controllers/items/items_update_controller/state.dart';
import 'package:nextrade/controllers/items/items_view_controller/controller.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/core/functions/upload_file.dart';
import 'package:nextrade/data/models/category_model.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class ItemsUpdateController extends GetxController {
  initData();
  updateItem();
  getCategories();
  toItemViewPage();
  showOptionsImage();
  chooseImageCamera();
  chooseImageGallery();
  changeStatus(val);
}

class ItemsUpdateControllerImp extends ItemsUpdateController {
  final state = ItemsUpdateState();
  @override
  void onInit() {
    initData();
    getCategories();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.statusRequest = StatusRequest.none;
    state.item = Get.arguments['item'];
    state.nameArController = TextEditingController();
    state.nameArController.text = state.item.nameAr!;
    state.deskArController = TextEditingController();
    state.deskArController.text = state.item.deskAr!;
    state.nameEnController = TextEditingController();
    state.nameEnController.text = state.item.nameEn!;
    state.deskEnController = TextEditingController();
    state.deskEnController.text = state.item.deskEn!;
    state.countController = TextEditingController();
    state.countController.text = '${state.item.count}';
    state.priceController = TextEditingController();
    state.priceController.text = '${state.item.price}';
    state.discountController = TextEditingController();
    state.discountController.text = '${state.item.discount}';
    state.categoryIdController = TextEditingController();
    state.categoryIdController.text = '${state.item.categoryId}';
    state.categoryNameController = TextEditingController();
    state.isActive = state.item.active!;
  }

  @override
  updateItem() async {
    try {
      if (state.formState.currentState!.validate()) {
        state.statusRequest = StatusRequest.loading;
        update();
        var response = await state.itemsData.updateItem(
          itemId: state.item.id!,
          active: '${state.isActive}',
          nameAr: state.nameArController.text,
          deskAr: state.deskArController.text,
          nameEn: state.nameEnController.text,
          deskEn: state.deskEnController.text,
          count: state.countController.text,
          discount: state.discountController.text,
          price: state.priceController.text,
          categoryId: state.categoryIdController.text,
          apiToken: state.apiToken,
          image: state.image,
        );
        state.statusRequest = handlingData(response);
        if (StatusRequest.success != state.statusRequest) {
          update();
          return;
        }
        if (response['status']) {
          if (state.categoryIdController.text == '${state.item.categoryId}') {
            ItemsViewControllerImp controller = Get.find();
            controller.getData();
          }
          toItemViewPage();
          update();
          return;
        }
        toastMessage(message: response['msg']);
      }
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  getCategories() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response =
          await state.categoriesData.getAllCategories(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        List temp = [];
        temp.addAll(response['data'].map((e) => Category.fromJson(e)));
        temp.forEach(
          (element) {
            state.categories.add(
              SelectedListItem(
                name: translateDatabase(element.nameAr, element.nameEn),
                value: '${element.id}',
              ),
            );
            if ('${element.id}' == state.categoryIdController.text) {
              state.categoryNameController.text =
                  translateDatabase(element.nameAr, element.nameEn);
            }
          },
        );
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toItemViewPage() {
    Get.back();
  }

  @override
  showOptionsImage() {
    showBottomMenu(chooseImageCamera, chooseImageGallery);
  }

  @override
  chooseImageCamera() async {
    state.image = await uploadImageCamera();
    update();
  }

  @override
  chooseImageGallery() async {
    state.image = await uploadImageGallery();
    update();
  }

  @override
  changeStatus(val) {
    state.isActive = val;
    update();
  }
}
