import 'package:get/get.dart';
import 'package:nextrade/controllers/items/items_view_controller/controller.dart';

class ItemsViewBindings extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<ItemsViewControllerImp>(() => ItemsViewControllerImp());
    Get.put(ItemsViewControllerImp());
  }
}
