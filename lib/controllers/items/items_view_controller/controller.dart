import 'package:nextrade/controllers/items/items_view_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/item_model.dart';
import 'package:nextrade/routes/routes_links.dart';

abstract class ItemsViewController extends GetxController {
  initData();
  getData();
  deleteItem(int itemId);
  toItemsCreatePage();
  toItemsUpdatePage(Item item);
}

class ItemsViewControllerImp extends ItemsViewController {
  final state = ItemsViewState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.categoryId = Get.arguments['categoryId'];
    state.statusRequest = StatusRequest.none;
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response =
          await state.itemsData.getItems(state.categoryId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.data.clear();
        state.data.addAll(response['data'].map((e) => Item.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  deleteItem(itemId) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.itemsData.deleteItem(itemId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.data.removeWhere((element) => element.id == itemId);
        if (state.data.isEmpty) state.statusRequest = StatusRequest.noData;
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toItemsCreatePage() {
    Get.toNamed(AppRoute.itemsCreate,
        arguments: {'categoryId': state.categoryId});
  }

  @override
  toItemsUpdatePage(item) {
    Get.toNamed(AppRoute.itemsUpdate, arguments: {'item': item});
  }
}
