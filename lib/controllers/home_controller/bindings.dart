import 'package:get/get.dart';
import 'package:nextrade/controllers/home_controller/controller.dart';

class HomeBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeControllerImp>(() => HomeControllerImp());
  }
}
