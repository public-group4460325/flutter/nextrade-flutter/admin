import 'package:get/get.dart';
import 'package:nextrade/controllers/home_controller/state.dart';
import 'package:nextrade/core/constants/routes.dart';

abstract class HomeController extends GetxController {
  toCategoriesViewPage();
  toUsersScreenPage();
  toOrdersPage();
}

class HomeControllerImp extends HomeController {
  final state = HomeState();

  @override
  toCategoriesViewPage() {
    Get.toNamed(AppRoute.categoriesView);
  }

  @override
  toUsersScreenPage() {
    Get.toNamed(AppRoute.usersScreen);
  }

  @override
  toOrdersPage() {
    Get.toNamed(AppRoute.adminOrdersScreen);
  }
}
