import 'routes_links.dart';

List<GetPage<dynamic>> routes = [
  GetPage(
    name: AppRoute.itemsUpdate,
    page: () => const ItemsUpdate(),
    binding: ItemsUpdateBindings(),
  ),
  GetPage(
    name: AppRoute.itemsCreate,
    page: () => const ItemsCreate(),
    binding: ItemsCreateBindings(),
  ),
  GetPage(
    name: AppRoute.categoriesUpdate,
    page: () => const CategoriesUpdate(),
    binding: CategoriesUpdateBindings(),
  ),
  GetPage(
    name: AppRoute.itemsView,
    page: () => const ItemsView(),
    binding: ItemsViewBindings(),
  ),
  GetPage(
    name: AppRoute.categoriesCreate,
    page: () => const CategoriesCreate(),
    binding: CategoriesCreateBindings(),
  ),
  GetPage(
    name: AppRoute.adminOrdersScreen,
    page: () => const AdminOrdersScreen(),
    binding: OrdersScreenBindings(),
  ),
  GetPage(
    name: AppRoute.adminHome,
    page: () => const AdminHome(),
    binding: HomeBindings(),
  ),
  GetPage(
    name: AppRoute.categoriesView,
    page: () => const CategoriesView(),
    binding: CategoriesViewBindings(),
  ),
  GetPage(
    name: '/',
    page: () => const Login(),
    binding: LoginBindings(),
    middlewares: [MyMiddleware()],
  ),
  GetPage(
    name: AppRoute.forgetPassword,
    page: () => const ForgetPassword(),
    binding: ForgetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.verifyCodeForgetPassword,
    page: () => const VerifyCodeForgetPassword(),
    binding: VerifyCodeForgetPasswordBindings(),
  ),GetPage(
    name: AppRoute.successResetPassword,
    page: () => const SuccessResetPassword(),
    binding: SuccessResetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.resetPassword,
    page: () => const ResetPassword(),
    binding: ResetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.orderDetails,
    page: () => const AdminOrderDetails(),
    binding: OrderDetailsBindings(),
  ),
  GetPage(
    name: AppRoute.usersScreen,
    page: () => const UsersScreen(),
    binding: UsersScreenBindings(),
  ),
];
