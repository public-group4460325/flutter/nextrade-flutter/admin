import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomTotalPrice extends StatelessWidget {
  final String price;
  const CustomTotalPrice({super.key, required this.price});

  @override
  Widget build(BuildContext context) {
    return Text(
      "${'total_price'.tr}: $price \$",
      textAlign: TextAlign.center,
      style: const TextStyle(
        color: AppColor.primaryColor,
        fontWeight: FontWeight.bold,
        fontSize: 18,
      ),
    );
  }
}
