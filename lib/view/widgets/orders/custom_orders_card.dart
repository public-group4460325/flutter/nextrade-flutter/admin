import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:nextrade/controllers/orders/pending_orders_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/data/models/order_model.dart';
import 'package:nextrade/view/widgets/orders/custom_order_button.dart';

class CustomOrdersCard extends GetView<PendingOrdersControllerImp> {
  final Order order;
  final void Function() onDetailsPressed;
  final void Function()? onApprovePressed;
  final void Function()? onPreparePressed;
  const CustomOrdersCard({
    required this.order,
    required this.onDetailsPressed,
    this.onApprovePressed,
    this.onPreparePressed,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(PendingOrdersControllerImp());
    return Card(
      child: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "${'order_id'.tr}: #${order.id}",
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  Jiffy.parse(order.createdAt!).fromNow(),
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: AppColor.primaryColor,
                  ),
                ),
              ],
            ),
            const Divider(),
            if (order.status != null)
              Text(
                '${'order_status'.tr}: ${controller.getOrderStatus("${order.status}")}',
              ),
            Text(
                "${'order_type'.tr}: ${controller.getReceiveType(order.receiveType!)}"),
            Text(
                "${'payment_method'.tr}: ${controller.getPaymentType(order.paymentType!)}"),
            Text("${'order_price'.tr}: ${order.price} \$"),
            Text("${'shipping'.tr}: ${order.shipping} \$"),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text(
                  "${'total_price'.tr}: ${order.totalPrice} \$",
                  style: const TextStyle(
                    fontWeight: FontWeight.bold,
                    color: AppColor.primaryColor,
                  ),
                ),
                Column(
                  children: [
                    CustomOrderButton(
                      text: 'order_details'.tr,
                      onPressed: onDetailsPressed,
                    ),
                    if (onApprovePressed != null)
                      CustomOrderButton(
                        text: 'approve_order'.tr,
                        onPressed: onApprovePressed!,
                      ),
                    if (onPreparePressed != null && order.status == 1)
                      CustomOrderButton(
                        text: 'prepare_order'.tr,
                        onPressed: onPreparePressed!,
                      ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
