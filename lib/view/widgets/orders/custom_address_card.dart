import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/data/models/address_model.dart';

class CustomAddressCard extends StatelessWidget {
  final Address address;
  const CustomAddressCard({super.key, required this.address});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        title: Text(
          'shipping_address'.tr,
          style: const TextStyle(fontSize: 20),
        ),
        subtitle: Row(
          children: [
            Text(
              '${address.name}: ',
              style: const TextStyle(fontSize: 18),
            ),
            Text(
              '${address.city} - ${address.street}',
              style: const TextStyle(fontSize: 15),
            ),
          ],
        ),
      ),
    );
  }
}
