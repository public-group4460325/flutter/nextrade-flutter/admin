import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/controllers/orders/order_details_controller/controller.dart';


class CustomMap extends GetView<OrderDetailsControllerImp> {
  const CustomMap({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(OrderDetailsControllerImp());
    return Card(
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        height: 400,
        width: double.infinity,
        child: GoogleMap(
          markers: controller.state.markers.toSet(),
          mapType: MapType.normal,
          initialCameraPosition: controller.state.kGooglePlex,
          onMapCreated: (GoogleMapController googleMapController) {
            controller.state.controller.complete(googleMapController);
          },
        ),
      ),
    );
  }
}
