import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomVerifyCodeField extends StatelessWidget {
  final void Function(String) onSubmit;
  const CustomVerifyCodeField({super.key, required this.onSubmit});

  @override
  Widget build(BuildContext context) {
    return OtpTextField(
      fieldWidth: AppDimensions.width / 7,
      borderRadius: BorderRadius.circular(20),
      numberOfFields: 5,
      borderColor: AppColor.primaryColor,
      showFieldAsBox: true,
      onSubmit: onSubmit,
    );
  }
}
