import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/view/screens/auth/forget_password/verify_code_forget_password.dart';

class CustomResendVerificationCodeButton
    extends GetView<VerifyCodeForgetPassword> {
  final void Function() onTap;
  const CustomResendVerificationCodeButton({super.key, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: onTap,
        child: Text(
          'resend_verification_code',
          style: TextStyle(
            color: AppColor.primaryColor,
            fontSize: AppDimensions.height / 50,
          ),
        ),
      ),
    );
  }
}
