import 'package:flutter/material.dart';

class CustomCard extends StatelessWidget {
  final void Function() onTap;
  final String title;
  final String imageUrl;
  const CustomCard({
    super.key,
    required this.onTap,
    required this.title,
    required this.imageUrl,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              imageUrl,
              width: 80,
            ),
            Text(title)
          ],
        ),
      ),
    );
  }
}
