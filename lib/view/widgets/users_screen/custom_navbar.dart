import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/users/users_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widgets/users_screen/custom_navbar_button.dart';

class CustomNavBar extends GetView<UsersScreenControllerImp> {
  const CustomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.backGroundColor,
      child: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          color: AppColor.backGroundColor,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ...List.generate(
                controller.state.pagesList.length,
                (index) => GetBuilder<UsersScreenControllerImp>(
                  builder: (controller) => CustomNavBarButton(
                    title: controller.state.bottomNavBarActions[index]['title'],
                    icon: controller.state.bottomNavBarActions[index]['icon'],
                    onPressed: () {
                      controller.changePage(index);
                    },
                    isActive: controller.state.currentPage == index,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
