// import 'package:flutter/material.dart';
// import 'package:get/get.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:nextrade/controller/deliveries/delivery_tracking_controller/controller.dart';

// class CustomMap extends GetView<DeliveryTrackingControllerImp> {
//   const CustomMap({super.key});

//   @override
//   Widget build(BuildContext context) {
//     Get.put(DeliveryTrackingControllerImp());
//     return Card(
//       child: GoogleMap(
//         markers: controller.state.markers.toSet(),
//         mapType: MapType.normal,
//         initialCameraPosition: controller.state.kGooglePlex,
//         onMapCreated: (GoogleMapController googleMapController) {
//           controller.state.controller = googleMapController;
//         },
//         polylines: controller.state.polylineSet,
//       ),
//     );
//   }
// }
