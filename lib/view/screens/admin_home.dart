import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/home_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/core/functions/exit_alert_app.dart';
import 'package:nextrade/view/widgets/home/custom_card.dart';

class AdminHome extends GetView<HomeControllerImp> {
  const AdminHome({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ExitAlertApp,
      child: Scaffold(
        appBar: AppBar(title: Text('home'.tr)),
        backgroundColor: AppColor.backGroundColor,
        body: ListView(
          children: [
            GridView(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisExtent: 150,
              ),
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              children: [
                CustomCard(
                  onTap: () => controller.toCategoriesViewPage(),
                  title: 'categories'.tr,
                  imageUrl: AppImageAsset.categories,
                ),
                CustomCard(
                  onTap: () => controller.toUsersScreenPage(),
                  title: 'users'.tr,
                  imageUrl: AppImageAsset.profile,
                ),
                CustomCard(
                  onTap: () => controller.toOrdersPage(),
                  title: 'orders'.tr,
                  imageUrl: AppImageAsset.orders,
                ),
                CustomCard(
                  onTap: () {},
                  title: 'reports'.tr,
                  imageUrl: AppImageAsset.reports,
                ),
                CustomCard(
                  onTap: () {},
                  title: 'notifications'.tr,
                  imageUrl: AppImageAsset.notifications,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
