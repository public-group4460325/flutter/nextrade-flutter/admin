import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/order_details_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/view/widgets/orders/custom_address_card.dart';
import 'package:nextrade/view/widgets/orders/custom_map.dart';
import 'package:nextrade/view/widgets/orders/custom_table.dart';
import 'package:nextrade/view/widgets/orders/custom_total_price.dart';

class AdminOrderDetails extends GetView<OrderDetailsControllerImp> {
  const AdminOrderDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: GetBuilder<OrderDetailsControllerImp>(
        builder: (controller) => HandlingDataView(
          statusRequest: controller.state.statusRequest,
          widget: ListView(
            children: [
              Card(
                child: Column(
                  children: [
                    const CustomTable(),
                    const SizedBox(height: 10),
                    CustomTotalPrice(price: '${controller.state.totalPrice}'),
                    const SizedBox(height: 10),
                  ],
                ),
              ),
              if (controller.state.address != null)
                CustomAddressCard(address: controller.state.address!),
              if (controller.state.address != null) const CustomMap(),
            ],
          ),
        ),
      ),
    );
  }
}
