import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/accepted_orders_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/view/widgets/orders/custom_orders_card.dart';

class AdminAcceptedOrders extends GetView<AcceptedOrdersControllerImp> {
  const AdminAcceptedOrders({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: GetBuilder<AcceptedOrdersControllerImp>(
        builder: (controller) => HandlingDataView(
          statusRequest: controller.state.statusRequest,
          widget: ListView.builder(
            itemCount: controller.state.orders.length,
            itemBuilder: (context, index) => CustomOrdersCard(
              order: controller.state.orders[index],
              onDetailsPressed: () => controller
                  .toOrdersDetailsPage(controller.state.orders[index]),
              onPreparePressed: () =>
                  controller.prepareOrder(controller.state.orders[index].id),
            ),
          ),
        ),
      ),
    );
  }
}
