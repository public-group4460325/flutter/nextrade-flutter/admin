import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/orders/orders_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widgets/admin_orders_screen/custom_nav_bar.dart';

class AdminOrdersScreen extends GetView<OrdersScreenControllerImp> {
  const AdminOrdersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OrdersScreenControllerImp>(
      builder: (controller) => Scaffold(
        appBar: AppBar(
          title: Text(controller.state
              .bottomNavBarActions[controller.state.currentPage]['title']),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        backgroundColor: AppColor.backGroundColor,
        bottomNavigationBar: const CustomNavBar(),
        body:
            controller.state.pagesList.elementAt(controller.state.currentPage),
      ),
    );
  }
}
