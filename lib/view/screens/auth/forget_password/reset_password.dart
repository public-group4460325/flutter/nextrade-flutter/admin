import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/auth/forget_password/reset_password_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';
import 'package:nextrade/view/widgets/auth/custom_text_caption.dart';
import 'package:nextrade/view/widgets/auth/custom_text_title.dart';

class ResetPassword extends GetView<ResetPasswordControllerImp> {
  const ResetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('reset_password'.tr)),
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 35),
        child: GetBuilder<ResetPasswordControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: Form(
              key: controller.state.formState,
              child: ListView(
                physics: const BouncingScrollPhysics(),
                children: [
                  Image.asset(AppImageAsset.resetPassword),
                  CustomTextTitle(title: 'new_password'.tr),
                  CustomTextCaption(caption: 'new_password_caption'.tr),
                  const SizedBox(height: 10),
                  CustomTextFormField(
                    onTapIcon: () => controller.showOrHidePassword(),
                    obscureText: controller.state.ishHidden,
                    isNumber: false,
                    validator: (val) {
                      return validationInput(val!, 8, 20, 'password');
                    },
                    hintText: 'password_caption'.tr,
                    labelText: 'password_label'.tr,
                    icon: controller.state.ishHidden
                        ? Icons.visibility_off
                        : Icons.visibility,
                    textEditingController: controller.state.passwordController,
                  ),
                  CustomTextFormField(
                    onTapIcon: () => controller.showOrHidePassword(),
                    obscureText: controller.state.ishHidden,
                    isNumber: false,
                    validator: (val) {
                      if (controller.state.passwordController.text !=
                          controller.state.confirmPasswordController.text) {
                        return 'not_match'.tr;
                      }
                      return validationInput(val!, 8, 20, 'password');
                    },
                    hintText: 're_password_caption'.tr,
                    labelText: 're_password_label'.tr,
                    icon: controller.state.ishHidden
                        ? Icons.visibility_off
                        : Icons.visibility,
                    textEditingController:
                        controller.state.confirmPasswordController,
                  ),
                  CustomButton(
                    text: 'save'.tr,
                    onPressed: () {
                      controller.resetPassword();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}