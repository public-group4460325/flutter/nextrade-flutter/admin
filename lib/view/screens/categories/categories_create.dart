import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/categories/categories_create_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';

class CategoriesCreate extends GetView<CategoriesCreateControllerImp> {
  const CategoriesCreate({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('add_category'.tr)),
      backgroundColor: AppColor.backGroundColor,
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<CategoriesCreateControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: Form(
              key: controller.state.formState,
              child: ListView(
                children: [
                  Image.asset(AppImageAsset.createCategory),
                  CustomTextFormField(
                    hintText: 'enter_category_name_in_arabic'.tr,
                    labelText: 'arabic_name'.tr,
                    icon: Icons.category,
                    textEditingController: controller.state.nameArController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_category_name_in_english'.tr,
                    labelText: 'english_name'.tr,
                    icon: Icons.category,
                    textEditingController: controller.state.nameEnController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: CustomButton(
                          text: controller.state.image == null
                              ? 'choose_image'.tr
                              : 'change_image'.tr,
                          onPressed: () => controller.chooseImage(),
                        ),
                      ),
                      const SizedBox(width: 15),
                      Expanded(
                        child: CustomButton(
                          text: 'create_category'.tr,
                          onPressed: () => controller.createCategory(),
                        ),
                      ),
                    ],
                  ),
                  if (controller.state.image != null)
                    Center(
                      child: SvgPicture.file(
                        height: 100,
                        controller.state.image!,
                      ),
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
