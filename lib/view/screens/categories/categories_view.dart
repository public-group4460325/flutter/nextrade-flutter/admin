import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:jiffy/jiffy.dart';
import 'package:nextrade/controllers/categories/categories_view_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/routes/routes_links.dart';

class CategoriesView extends GetView<CategoriesViewControllerImp> {
  const CategoriesView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('categories'.tr),
        automaticallyImplyLeading: false,
      ),
      backgroundColor: AppColor.backGroundColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () => controller.toCategoryCreatePage(),
        child: const Icon(Icons.add),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<CategoriesViewControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: ListView.builder(
              itemCount: controller.state.data.length,
              itemBuilder: (context, index) => InkWell(
                onTap: () =>
                    controller.toItemsViewPage(controller.state.data[index].id),
                child: Card(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: SvgPicture.network(
                            '${AppLinkServer.storage}/${controller.state.data[index].image}',
                            height: 70,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: ListTile(
                          title: Text(
                            translateDatabase(
                                controller.state.data[index].nameAr!,
                                controller.state.data[index].nameEn!),
                          ),
                          subtitle: Text(
                            Jiffy.parse(controller.state.data[index].createdAt!)
                                .fromNow(),
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              IconButton(
                                onPressed: () => controller.deleteCategory(
                                    controller.state.data[index].id),
                                icon: const Icon(Icons.delete),
                              ),
                              IconButton(
                                onPressed: () =>
                                    controller.toCategoriesUpdatePage(
                                  controller.state.data[index].id,
                                ),
                                icon: const Icon(Icons.edit),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
