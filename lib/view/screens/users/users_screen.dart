import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/users/users_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widgets/users_screen/custom_navbar.dart';

class UsersScreen extends GetView<UsersScreenControllerImp> {
  const UsersScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UsersScreenControllerImp>(
      builder: (controller) => Scaffold(
        backgroundColor: AppColor.backGroundColor,
        bottomNavigationBar: const CustomNavBar(),
        body:
            controller.state.pagesList.elementAt(controller.state.currentPage),
      ),
    );
  }
}
