import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controllers/items/items_create_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_drop_down_menu.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';

class ItemsCreate extends GetView<ItemsCreateControllerImp> {
  const ItemsCreate({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('add_item'.tr)),
      backgroundColor: AppColor.backGroundColor,
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<ItemsCreateControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: Form(
              key: controller.state.formState,
              child: ListView(
                children: [
                  CustomTextFormField(
                    hintText: 'enter_item_name_in_arabic'.tr,
                    labelText: 'arabic_name'.tr,
                    icon: Icons.category,
                    textEditingController: controller.state.nameArController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_item_desk_in_arabic'.tr,
                    labelText: 'arabic_desk'.tr,
                    icon: Icons.description,
                    textEditingController: controller.state.deskArController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_item_name_in_english'.tr,
                    labelText: 'english_name'.tr,
                    icon: Icons.category,
                    textEditingController: controller.state.nameEnController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_item_desk_in_english'.tr,
                    labelText: 'english_desk'.tr,
                    icon: Icons.description,
                    textEditingController: controller.state.deskEnController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_the_price'.tr,
                    labelText: 'Price'.tr,
                    icon: Icons.price_change,
                    textEditingController: controller.state.priceController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: true,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_the_count'.tr,
                    labelText: 'count'.tr,
                    icon: Icons.numbers,
                    textEditingController: controller.state.countController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: true,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_the_discount'.tr,
                    labelText: 'discount'.tr,
                    icon: Icons.discount,
                    textEditingController: controller.state.discountController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: true,
                  ),
                  CustomDropDownMenu(
                    label: 'category'.tr,
                    hint: 'select_category'.tr,
                    selectedIdController: controller.state.categoryIdController,
                    selectedNameController:
                        controller.state.categoryNameController,
                    data: controller.state.categories,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: CustomButton(
                          text: controller.state.image == null
                              ? 'choose_image'.tr
                              : 'change_image'.tr,
                          onPressed: () => controller.showOptionsImage(),
                        ),
                      ),
                      const SizedBox(width: 15),
                      Expanded(
                        child: CustomButton(
                          text: 'create_item'.tr,
                          onPressed: () => controller.createItem(),
                        ),
                      ),
                    ],
                  ),
                  if (controller.state.image != null)
                    Center(
                      child: Image.file(
                        height: 100,
                        controller.state.image!,
                      ),
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
