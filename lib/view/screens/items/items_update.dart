import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:nextrade/controllers/items/items_update_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/constants/lottie_assets.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_drop_down_menu.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';

class ItemsUpdate extends GetView<ItemsUpdateControllerImp> {
  const ItemsUpdate({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('update_item'.tr)),
      backgroundColor: AppColor.backGroundColor,
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<ItemsUpdateControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: Form(
              key: controller.state.formState,
              child: ListView(
                children: [
                  CustomTextFormField(
                    hintText: 'enter_item_name_in_arabic'.tr,
                    labelText: 'arabic_name'.tr,
                    icon: Icons.category,
                    textEditingController: controller.state.nameArController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_item_desk_in_arabic'.tr,
                    labelText: 'arabic_desk'.tr,
                    icon: Icons.description,
                    textEditingController: controller.state.deskArController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_item_name_in_english'.tr,
                    labelText: 'english_name'.tr,
                    icon: Icons.category,
                    textEditingController: controller.state.nameEnController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_item_desk_in_english'.tr,
                    labelText: 'english_desk'.tr,
                    icon: Icons.description,
                    textEditingController: controller.state.deskEnController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: false,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_the_price'.tr,
                    labelText: 'Price'.tr,
                    icon: Icons.price_change,
                    textEditingController: controller.state.priceController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: true,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_the_count'.tr,
                    labelText: 'count'.tr,
                    icon: Icons.numbers,
                    textEditingController: controller.state.countController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: true,
                  ),
                  CustomTextFormField(
                    hintText: 'enter_the_discount'.tr,
                    labelText: 'discount'.tr,
                    icon: Icons.discount,
                    textEditingController: controller.state.discountController,
                    validator: (val) => validationInput(val!, 1, 30, ''),
                    isNumber: true,
                  ),
                  CustomDropDownMenu(
                    label: 'category'.tr,
                    hint: 'select_category'.tr,
                    selectedIdController: controller.state.categoryIdController,
                    selectedNameController:
                        controller.state.categoryNameController,
                    data: controller.state.categories,
                  ),
                  RadioListTile(
                    title: Text('active'.tr),
                    value: 1,
                    groupValue: controller.state.isActive,
                    onChanged: (val) => controller.changeStatus(val),
                  ),
                  RadioListTile(
                    title: Text('hide'.tr),
                    value: 0,
                    groupValue: controller.state.isActive,
                    onChanged: (val) => controller.changeStatus(val),
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: CustomButton(
                          text: 'change_image'.tr,
                          onPressed: () => controller.showOptionsImage(),
                        ),
                      ),
                      const SizedBox(width: 15),
                      Expanded(
                        child: CustomButton(
                          text: 'update_item'.tr,
                          onPressed: () => controller.updateItem(),
                        ),
                      ),
                    ],
                  ),
                  Center(
                    child: controller.state.image != null
                        ? Image.file(
                            controller.state.image!,
                            height: 100,
                          )
                        : CachedNetworkImage(
                            imageUrl:
                                '${AppLinkServer.storage}/${controller.state.item.image}',
                            height: 100,
                            placeholder: (context, url) => Center(
                              child: Lottie.asset(
                                AppLottieAsset.loading,
                              ),
                            ),
                          ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
