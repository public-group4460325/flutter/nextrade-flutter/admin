import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:jiffy/jiffy.dart';
import 'package:lottie/lottie.dart';
import 'package:nextrade/controllers/items/items_view_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/constants/lottie_assets.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/routes/routes_links.dart';

class ItemsView extends GetView<ItemsViewControllerImp> {
  const ItemsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('items'.tr),
        automaticallyImplyLeading: false,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => controller.toItemsCreatePage(),
        child: const Icon(Icons.add),
      ),
      backgroundColor: AppColor.backGroundColor,
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<ItemsViewControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: ListView.builder(
              itemCount: controller.state.data.length,
              itemBuilder: (context, index) => InkWell(
                onTap: () =>
                    controller.toItemsUpdatePage(controller.state.data[index]),
                child: Card(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 2,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: CachedNetworkImage(
                            imageUrl:
                                '${AppLinkServer.storage}/${controller.state.data[index].image}',
                            placeholder: (context, url) => Center(
                              child: Lottie.asset(
                                AppLottieAsset.loading,
                              ),
                            ),
                            height: 70,
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 3,
                        child: ListTile(
                          title: Text(
                            translateDatabase(
                                controller.state.data[index].nameAr!,
                                controller.state.data[index].nameEn!),
                          ),
                          subtitle: Text(
                            Jiffy.parse(controller.state.data[index].createdAt!)
                                .fromNow(),
                          ),
                          trailing: IconButton(
                            onPressed: () => controller.deleteItem(
                              controller.state.data[index].id,
                            ),
                            icon: const Icon(Icons.delete),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
