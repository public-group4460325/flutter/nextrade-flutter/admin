import 'package:drop_down_list/drop_down_list.dart';
import 'package:drop_down_list/model/selected_list_item.dart';
import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomDropDownMenu extends StatefulWidget {
  final String label;
  final String hint;
  final List<SelectedListItem> data;
  final TextEditingController selectedNameController;
  final TextEditingController selectedIdController;
  const CustomDropDownMenu({
    super.key,
    required this.label,
    required this.hint,
    required this.data,
    required this.selectedNameController,
    required this.selectedIdController,
  });

  @override
  State<CustomDropDownMenu> createState() => _CustomDropDownMenuState();
}

class _CustomDropDownMenuState extends State<CustomDropDownMenu> {
  void showDropDownSearch() {
    DropDownState(
      DropDown(
        isDismissible: true,
        bottomSheetTitle: Text(
          widget.label,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20.0,
          ),
        ),
        submitButtonChild: const Text(
          'Done',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
          ),
        ),
        data: widget.data,
        selectedItems: (List<dynamic> selectedList) {
          SelectedListItem selectedListItem = selectedList[0];
          widget.selectedNameController.text = selectedListItem.name;
          widget.selectedIdController.text = selectedListItem.value!;
        },
      ),
    ).showModal(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: AppDimensions.height / 40,
        top: AppDimensions.height / 80,
      ),
      child: TextFormField(
        controller: widget.selectedNameController,
        cursorColor: Colors.black,
        onTap: () {
          FocusScope.of(context).unfocus();
          showDropDownSearch();
        },
        decoration: InputDecoration(
          suffixIcon: const Icon(Icons.arrow_drop_down),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          label: Container(
            margin: EdgeInsets.symmetric(horizontal: AppDimensions.width / 30),
            child: Text(widget.label),
          ),
          hintText: widget.hint,
          contentPadding: EdgeInsets.symmetric(
            vertical: AppDimensions.height / 70,
            horizontal: AppDimensions.width / 20,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          filled: true,
          fillColor: AppColor.textFormColor,
        ),
      ),
    );
  }
}
