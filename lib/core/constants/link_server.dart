class AppLinkServer {
  // Server
  static const String server = "http://10.0.2.2:8000";
  static const String auth = "$server/api/auth";
  static const String users = "$server/api/users";
  static const String admins = "$server/api/admins";
  static const String deliveries = "$server/api/deliveries";
  static const String storage = "$server/storage";

  // ================================= Auth ==================================
  static const String login = '$auth/login';
  static const String logout = '$auth/logout';
  static const String resendVerificationCode = '$auth/resendVerificationCode';

  // Forget Password
  static const String resetPassword = '$auth/resetPassword';
  static const String checkEmail = '$auth/checkEmail';
  static const String verifyCodeForgetPassword =
      '$auth/verifyCodeForgetPassword';
  // ========================================================================

  // ================================ Admins ================================
  // Categories
  static const String deleteCategory = '$admins/categories/delete';
  static const String updateCategory = '$admins/categories/update';
  static const String createCategory = '$admins/categories/create';
  static const String getAllCategories = '$admins/categories/get';

  //Items
  static const String createItem = '$admins/items/create';
  static const String updateItem = '$admins/items/update';
  static const String deleteItem = '$admins/items/delete';
  static const String getItemsByCategoryAdmin = '$admins/items/get';

  // Orders
  static const String approveOrder = '$admins/orders/approve';
  static const String prepareOrder = '$admins/orders/prepare';
  static const String getArchivedOrdersAdmin = '$admins/orders/getArchived';
  static const String getPendingOrdersAdmin = '$admins/orders/getPending';
  static const String getAcceptedOrdersAdmin = '$admins/orders/getAccepted';
  static const String getDetailsOrderAdmin = '$admins/orders/getDetails';
  // ========================================================================
}
