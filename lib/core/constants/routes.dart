class AppRoute {
  // Auth
  static const String login = '/login';
  static const String forgetPassword = '/forgetPassword';
  static const String verifyCodeForgetPassword = '/verifyCodeForgetPassword';
  static const String resetPassword = '/resetPassword';
  static const String successResetPassword = '/successResetPassword';
  // OnBoarding
  static const String onBoarding = '/onBoarding';
  static const String language = '/language';

  static const String adminOrdersScreen = '/adminOrderScreen';
  static const String adminHome = '/adminHome';

  static const String categoriesView = '/categoriesView';
  static const String categoriesCreate = '/categoriesCreate';
  static const String categoriesUpdate = '/categoriesUpdate';
  static const String itemsView = '/itemsView';
  static const String itemsCreate = '/itemsCreate';
  static const String itemsUpdate = '/itemsUpdate';

  
  static const String orderDetails = '/orderDetails';
  static const String usersScreen = '/usersScreen';
}
