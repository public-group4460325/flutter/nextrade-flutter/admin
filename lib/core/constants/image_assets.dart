class AppImageAsset {
  static const String rootImage = 'assets/images';
  // Auth
  static const String logo = '$rootImage/auth/Logo.png';
  static const String forgetPassword = '$rootImage/auth/forget_password.png';
  static const String resetPassword = '$rootImage/auth/reset_password.png';
  static const String verifyCode = '$rootImage/auth/verify_code.png';

  static const String profile = '$rootImage/profile.png';
  static const String categories = '$rootImage/categories.png';
  static const String orders = '$rootImage/orders.png';
  static const String reports = '$rootImage/reports.png';
  static const String notifications = '$rootImage/notifications.png';
  static const String createCategory = '$rootImage/create_category.png';
}
