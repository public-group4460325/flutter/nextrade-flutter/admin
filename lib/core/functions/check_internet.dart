import 'dart:io';

import 'package:flutter/foundation.dart';

Future<bool> CheckInternet() async {
  return true;
  try {
    var res = await InternetAddress.lookup('google.com');
    return res.isNotEmpty && res[0].rawAddress.isNotEmpty;
  } on SocketException catch (e) {
    if (kDebugMode) print(e);
    return false;
  }
}
