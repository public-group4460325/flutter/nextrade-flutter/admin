import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../constants/lottie_assets.dart';
import 'status_request.dart';

class HandlingDataRequest extends StatelessWidget {
  final StatusRequest statusRequest;
  final Widget widget;
  const HandlingDataRequest({
    super.key,
    required this.statusRequest,
    required this.widget,
  });

  @override
  Widget build(BuildContext context) {
    return statusRequest == StatusRequest.loading
        ? Center(
            child: Lottie.asset(
              AppLottieAsset.loading,
              width: Get.width / 2,
              height: Get.width / 2,
            ),
          )
        : statusRequest == StatusRequest.offLineFailed
            ? Center(
                child: Lottie.asset(
                  AppLottieAsset.connectionLost,
                  width: Get.width / 2,
                  height: Get.width / 2,
                ),
              )
            : statusRequest == StatusRequest.serverFailed
                ? Center(
                    child: Lottie.asset(
                      AppLottieAsset.error,
                      width: Get.width / 2,
                      height: Get.width / 2,
                    ),
                  )
                : widget;
  }
}
