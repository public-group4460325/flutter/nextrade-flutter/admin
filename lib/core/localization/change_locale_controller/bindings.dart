import 'package:get/get.dart';

import 'controller.dart';

class ChangeLanguageBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ChangeLocaleControllerImp>(() => ChangeLocaleControllerImp());
  }
}
