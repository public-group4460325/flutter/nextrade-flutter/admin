import '../../../../core/classes/crud.dart';
import '../../../../core/constants/link_server.dart';

class LoginData {
  CRUD crud;
  LoginData(this.crud);
  postData({
    required String email,
    required String password,
  }) async {
    var body = {
      'email': email,
      'password': password,
    };
    var response =
        await crud.postData(linkUrl: AppLinkServer.login, body: body);
    return response.fold((l) => l, (r) => r);
  }
}
