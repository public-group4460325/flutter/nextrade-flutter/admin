import 'dart:io';

import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class ItemsData {
  CRUD crud;
  ItemsData(this.crud);
  getItems(int categoryId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getItemsByCategoryAdmin,
      body: {'category_id': '$categoryId'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  createItem({
    required String nameAr,
    required String deskAr,
    required String nameEn,
    required String deskEn,
    required String count,
    required String price,
    required String discount,
    required String categoryId,
    required String apiToken,
    required File image,
  }) async {
    Map body = {
      'name_ar': nameAr,
      'desk_ar': deskAr,
      'name_en': nameEn,
      'desk_en': deskEn,
      'count': count,
      'price': price,
      'discount': discount,
      'category_id': categoryId,
    };
    var response = await crud.addRequestWithImageOne(
      AppLinkServer.createItem,
      body,
      image,
      {'Authorization': 'Bearer $apiToken'},
      'image',
    );
    return response.fold((l) => l, (r) => r);
  }

  updateItem({
    required int itemId,
    required String nameAr,
    required String deskAr,
    required String nameEn,
    required String deskEn,
    required String count,
    required String active,
    required String price,
    required String discount,
    required String categoryId,
    required String apiToken,
    File? image,
  }) async {
    Map body = {
      'name_ar': nameAr,
      'desk_ar': deskAr,
      'name_en': nameEn,
      'desk_en': deskEn,
      'count': count,
      'active': active,
      'price': price,
      'discount': discount,
      'category_id': categoryId,
      'item_id': '$itemId',
    };
    var response;
    if (image == null) {
      response = await crud.postData(
        linkUrl: AppLinkServer.updateItem,
        headers: {'Authorization': 'Bearer $apiToken'},
        body: body,
      );
    } else {
      response = await crud.addRequestWithImageOne(
        AppLinkServer.updateItem,
        body,
        image,
        {'Authorization': 'Bearer $apiToken'},
        'image',
      );
    }
    return response.fold((l) => l, (r) => r);
  }

  deleteItem(int itemId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.deleteItem,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'item_id': '$itemId'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
