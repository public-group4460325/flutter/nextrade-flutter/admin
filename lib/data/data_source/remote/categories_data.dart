import 'dart:io';

import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class CategoriesData {
  CRUD crud;
  CategoriesData(this.crud);
  getAllCategories(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getAllCategories,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  createCategory({
    required String nameAr,
    required String nameEn,
    required String apiToken,
    required File image,
  }) async {
    Map body = {
      'name_ar': nameAr,
      'name_en': nameEn,
    };
    var response = await crud.addRequestWithImageOne(
      AppLinkServer.createCategory,
      body,
      image,
      {'Authorization': 'Bearer $apiToken'},
      'image',
    );
    return response.fold((l) => l, (r) => r);
  }

  updateCategory({
    required int categoryId,
    required String nameAr,
    required String nameEn,
    required String apiToken,
    File? image,
  }) async {
    Map body = {
      'name_ar': nameAr,
      'name_en': nameEn,
      'category_id': '$categoryId',
    };
    var response;
    if (image == null) {
      response = await crud.postData(
        linkUrl: AppLinkServer.updateCategory,
        headers: {'Authorization': 'Bearer $apiToken'},
        body: body,
      );
    } else {
      response = await crud.addRequestWithImageOne(
        AppLinkServer.updateCategory,
        body,
        image,
        {'Authorization': 'Bearer $apiToken'},
        'image',
      );
    }
    return response.fold((l) => l, (r) => r);
  }

  deleteCategory(int categoryId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.deleteCategory,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'category_id': '$categoryId'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
