import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class OrdersData {
  CRUD crud;
  OrdersData(this.crud);
  approveOrder(int orderId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.approveOrder,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'order_id': '$orderId'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getPendingOrders(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getPendingOrdersAdmin,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getAcceptedOrders(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getAcceptedOrdersAdmin,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  prepareOrder(int orderId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.prepareOrder,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'order_id': '$orderId'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getArchivedOrders(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getArchivedOrdersAdmin,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getDetailsOrder(int orderId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getDetailsOrderAdmin,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'order_id': '$orderId'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
