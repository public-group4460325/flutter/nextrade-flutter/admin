class Item {
  int? id;
  String? nameAr;
  String? deskAr;
  String? nameEn;
  String? deskEn;
  int? count;
  int? active;
  double? price;
  int? discount;
  String? image;
  int? categoryId;
  String? createdAt;
  String? updatedAt;
  bool? favorite;

  Item(
      {this.id,
      this.nameAr,
      this.deskAr,
      this.nameEn,
      this.deskEn,
      this.count,
      this.active,
      this.price,
      this.discount,
      this.image,
      this.categoryId,
      this.createdAt,
      this.updatedAt,
      this.favorite});

  Item.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    deskAr = json['desk_ar'];
    nameEn = json['name_en'];
    deskEn = json['desk_en'];
    count = int.parse('${json['count']}');
    active = json['active'];
    price = double.parse("${json['price']}");
    discount = json['discount'];
    image = json['image'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    favorite = json['favorite'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name_ar'] = nameAr;
    data['desk_ar'] = deskAr;
    data['name_en'] = nameEn;
    data['desk_en'] = deskEn;
    data['count'] = count;
    data['active'] = active;
    data['price'] = price;
    data['discount'] = discount;
    data['image'] = image;
    data['category_id'] = categoryId;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['favorite'] = favorite;
    return data;
  }
}
