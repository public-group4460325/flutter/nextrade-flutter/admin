class Order {
  int? id;
  int? userId;
  int? addressId;
  String? receiveType;
  String? paymentType;
  double? shipping;
  double? price;
  double? totalPrice;
  int? couponId;
  int? status;
  String? createdAt;
  String? updatedAt;
  bool? rated;

  Order({
    this.id,
    this.userId,
    this.addressId,
    this.receiveType,
    this.paymentType,
    this.shipping,
    this.price,
    this.totalPrice,
    this.couponId,
    this.status,
    this.createdAt,
    this.updatedAt,
    this.rated,
  });

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    addressId = json['address_id'];
    receiveType = json['receive_type'];
    paymentType = json['payment_type'];
    shipping = double.parse("${json['shipping']}");
    price = double.parse("${json['price']}");
    totalPrice = double.parse("${json['total_price']}");
    couponId = json['coupon_id'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    rated = json['rated'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['user_id'] = userId;
    data['address_id'] = addressId;
    data['receive_type'] = receiveType;
    data['payment_type'] = paymentType;
    data['shipping'] = shipping;
    data['price'] = price;
    data['total_price'] = totalPrice;
    data['coupon_id'] = couponId;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['rated'] = rated;
    return data;
  }
}
